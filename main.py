import numpy as np
import matplotlib.pyplot as plt
from scipy.misc import imread

# import poisson and image editing functions
from poisson import *

def select_image():
    """
    Promts the user to select an image and converts it to float64

    Arguments:
        None

    Returns:
        image
    """
    while True:
        try:
            # prompt user for image path
            path = input('Select image (path): ')
            # read path as image
            image = imread(path).astype(float)/255
            # TODO: implement regex for image check
            break
        except FileNotFoundError:
            # read error, ask user again
            print('Not a valid path or image. Try again: ')
    return image

# TODO: Implement this functionality if I have the time...
def promt_save():
    ans = input("Would you like to save this image? (Y/n): ")
    while True:
        if ans == "Y":
            pass
            break
        elif ans == 'n':
            pass
            break
        else:
            ans = input("Select Y/n: ")

# image manipulation choices
choices = ['Exit',
           'Blur',
           'Contrast Linear',
           'Contrast Non-linear',
           'Grayscale',
           'Facial Blur',
           'Inpainting',
           'Demoisaic',
           'Edge Preserving Blur',
           'Clone',
           'Print menu'
           ]

def menu():
    print('\n')
    print('{:{align}{width}}'.format('MENU', align='^', width='40'))
    print('#'*41)
    for index,item in enumerate(choices):
        print('{:20} {:>20}'.format('%s' % item, '%d' % index))

def display(im1, *args):
    if len(args) == 0:
        plt.imshow(im1)
        plt.axis('off')
        plt.show()
    elif len(args) == 1:
        plt.subplot(121)
        plt.imshow(im1)
        plt.axis('off')
        plt.subplot(122)
        plt.imshow(im2)
        plt.axis('off')
        plt.show()
    elif len(args) == 2:
        plt.subplot(221)
        plt.imshow(im1)
        plt.axis('off')
        plt.subplot(222)
        plt.imshow(im2)
        plt.axis('off')
        plt.subplot(212)
        plt.imshow(im3)
        plt.axis('off')
        plt.show()
    else:
        print("Something went wrong...")

# print menu
menu()

# program loop
while True:
    last = len(choices) - 1
    choice = input('\nYour choice 0 - %d (0 to exit): ' % last)
    if choice == '0':
        break
    # blur
    elif choice == '1':
        image = select_image()
        im1 = blur(image)
        display(im1)
    # contrast linear
    elif choice == '2':
        image = select_image()
        im1, im2 = contrast_linear(image)
        display(im1, im2)
    # contrast non-linear
    elif choice == '3':
        image = select_image()
        contrast_nonlinear(image)
    # grayscale
    elif choice == '4':
        image = select_image()
        grayscale(image)
    # facial blur
    elif choice == '5':
        image = select_image()
        im1 = facial_blur(image)
        display(im1)
    # inpainting demonstration
    elif choice == '6':
        image = select_image()
        im1, im2, im3 = inpainting_demo(image)
        display(im1, im2, im3)
    # demosaic
    elif choice == '7':
        image = select_image()
        im1, im2 = demosaicing(image)
        display(im1, im2)
    # edge preserving blur
    elif choice == '8':
        image = select_image()
        im1, im2 = EdgePreservingBlur(image)
        display(im1, im2)
    # seamless cloning
    elif choice == '9':
        # demonstration purposes only, functionality reduced
        clone(None, None, None, None, dt=.2)
    elif choice == '10':
        menu()
    else:
        print('Please select a valid option.\n')
