import matplotlib.pyplot as plt
from scipy.misc import imread
import numpy as np
from numpy import sqrt
import cv2

# TODO: image is kept after editing, let user keep or discard image after
#       function is applied

# check:
#   raise ValueError(<string;desc>)

def laplacian(im):
    """
    calculate and return the laplacian of given image

    Args:
        im: image matrix
    Returns:
        l: the calculated laplacian of image matrix
    """
    l = np.zeros(im.shape)
    l[1:-1, 1:-1] = (im[0:-2, 1:-1] + im[2:, 1:-1] +
                     im[1:-1, 0:-2] + im[1:-1, 2:] -
                     4*im[1:-1, 1:-1])

    l[:, 0] = l[:, 1]      # Neumann boundary
    l[:, -1] = l[:, -2]    #
    l[0, :] = l[1, :]      #
    l[-1, :] = l[-2 , :]   #

    return l

def gradient(arr, d):
    """
    calculate and return the gradient in x and y direaction

    Args:
        arr: array; matrix like object
        d:   char; forward or backward difference approximation
    Returns:
        g_x, g_y: the gradient for x and y as array-like objects
    """
    g_x = np.zeros(arr.shape)
    g_y = np.zeros(arr.shape)

    if d=='f':
        g_x[:, 0:-1] = arr[: , 1:] - arr[: , 0:-1]
        g_y[0:-1, :] = arr[1: ,:] - arr[0:-1, :]
        g_x[:, -1]   = g_x[:, -2]
        g_y[-1, :]   = g_y[-2 , :]
    elif d=='b':
        g_x[:, 1:] = arr[: ,1:] - arr[:, 0:-1]
        g_y[1:, :] = arr[1: ,:] - arr[0:-1, :]
        g_x[:, 0]  = g_x[:, 1]
        g_y[0, :]  = g_y[1 , :]

    return g_x, g_y


def contrast_linear(im, k=2, n=5, dt=0.2):
    """
    This function increases the contrast of the image using a
    passed constant k. This function uses the linear way of calculating
    the contrast

    Arguments:
        im: ndarray(float64); source image to be manipulated
        k:  int;              contrast constant
        n:  int;              number of loop iterations
        dt: float64;          delta constant of poisson

    Returns:
        Original image and the edited image

    """
    original = im.copy()
    u0 = laplacian(im)*k
    for _ in range(n):
        u = laplacian(im)
        im += dt*(u - u0)
        im[im > 1] = 1
        im[im < 0] = 0

    return original, im

def contrast_nonlinear(im, n=2, dt=0.2):
    """
    Calculates the contrast using the nonlinear equation as described in the
    assignment.

    Args:
        im: user provided image
        n:  number of loop iterations
        dt: delta_t

    Returns:
        None. Shows user the original image and the edited image
    """
    original = im.copy()

    g_x, g_y = gradient(im, 'f')

    g_len = sqrt(g_x**2 + g_y**2)
    g_len[g_len == 0] = np.finfo(float).eps
    ex, ey = g_x/g_len, g_y/g_len
    g_x, g_y = ex*sqrt(g_len), ey*sqrt(g_len)

    g_xx, _ = gradient(g_x, 'b') # catch only g_x
    _, g_yy = gradient(g_y, 'b') # catch only g_y

    h = g_xx + g_yy

    for _ in range(n):
        l = laplacian(im)
        im += dt*(l-h)

        im[:, 0]  = im[:, 1]      # Neumann boundary
        im[:, -1] = im[:, -2]     #
        im[0, :]  = im[1, :]      #
        im[-1, :] = im[-2 , :]    #

        im[im > 1] = 1
        im[im < 0] = 0

    plt.subplot(121)
    plt.imshow(original, plt.cm.gray)
    plt.axis('off')
    plt.subplot(122)
    plt.imshow(im, plt.cm.gray)
    plt.axis('off')

    plt.show()

def grayscale(im, dt=0.2, n=100): # fast Color2 Gray
    """
    Create a grayscale image using the weighted average of the different
    R, G and B values. By using this method we can keep more information as
    we are calculating the gradient rather than just using a mean value for
    all the pixels.

    Args:
        im: ndarray(float64); source image
        n:  int; loop iteration constant
        dt: int; delta constant poisson
    Returns:
        None. Displays greyscale image for user
    """
    original = im.copy().mean(axis=2)         # for comparison

    # gets the gradient of RGB values of x and y directions
    g_r_x, g_r_y = gradient(im[:, :, 0], 'f') # get r values x and y
    g_g_x, g_g_y = gradient(im[:, :, 1], 'f') # get g values x and y
    g_b_x, g_b_y = gradient(im[:, :, 2], 'f') # get b values x and y

    # calculates the gradient length using the formula given
    # in the project
    g_len = sqrt(
        (g_r_x**2 + g_r_y**2 +
         g_g_x**2 + g_g_y**2 +
         g_b_x**2 + g_b_y**2) / 3
    )

    # gets the weighted sum of RGB values in pixel
    color_sum = im[:,:,0] + im[:,:,1] + im[:,:,2]

    g_c_x, g_c_y  = gradient(color_sum, 'f')
    c_len = sqrt(g_c_x**2 + g_c_y**2)

    # approximate 0 to the lowest float value supported by the computer
    c_len[c_len == 0] = np.finfo(float).eps

    # calculates the unit vectors
    ex, ey = g_c_x/c_len, g_c_y/c_len
    g_x, g_y = ex*g_len, ey*g_len

    # run the gradient again using backward difference approximation
    g_xx, _ = gradient(g_x, 'b') # catch only g_x
    _, g_yy = gradient(g_y, 'b') # catch only g_y

    # calculates the h constant to be used in the final function
    h = g_xx + g_yy

    # converts image to simple grayscale using mean method
    im = im.mean(axis=2)
    for _ in range(n):
        l = laplacian(im)
        im += dt*(l-h)

        im[:, 0]  = im[:, 1]      # Neumann boundary
        im[:, -1] = im[:, -2]     #
        im[0, :]  = im[1, :]      #
        im[-1, :] = im[-2 , :]    #

        # handle special cases for pixel value > 1 and < 0
        im[im > 1] = 1
        im[im < 0] = 0

    plt.subplot(121)
    plt.imshow(original, plt.cm.gray)
    plt.axis('off')
    plt.subplot(122)
    plt.imshow(im, plt.cm.gray)
    plt.axis('off')

    plt.show()

def blur(im, n=10, dt=0.2):
    """
    Blurs the passed image using the blur equation

    Arguments:
        im  : ndarray(float64); target image (array-like)
        n   : int;              number of loop iterations
        dt  : int;              poisson delta number

    Returns:
        Edited image (ndarray)
    """

    for _ in range(n):
        l = laplacian(im)
        im += dt*l

    return im

def EdgePreservingBlur(im, n=20, dt=0.2):
    """
    BLurs the image but keeps the edges intact using the gradient

    Arguments:
        im: ndarray(float64): user provided image
        n : int: number of iterations to perform blur
        dt: float: constant, usually 0.2

    Returns:
        iriginal image and edited image
    """
    original = im.copy()
    G_x, G_y = gradient(im, 'f')
    k = 10000

    GradientLength = G_x**2 + G_y**2
    D = 1/(1 + k*GradientLength)

    for _ in range(n):
        G_x, G_y = gradient(im, 'f')
        G_x, G_y = G_x*D, G_y*D
        G_xx, _ = gradient(G_x, 'b') # catch only g_x
        _, G_yy = gradient(G_y, 'b') # catch only g_y

        L = G_xx + G_yy
        im += L*dt
        im[im > 1] = 1
        im[im < 0] = 0
        im[:, 0]  = im[:, 1]      # Neumann boundary
        im[:, -1] = im[:, -2]     #
        im[0, :]  = im[1, :]      #
        im[-1, :] = im[-2 , :]    #

    return original, im

def facial_blur(im):
    """
    This funtion Uses OpenCV to detect faces in images and blurs them.
    The faces are detected using a haar cascade classifier, described in
    the project

    Arguments:
        im: ndarray(float64); target image

    Returns:
        edited image
    """

    # make a copy of the original image to be used later
    image = im.copy()

    # convert image to uint8
    if "float" in str(im.dtype):
        im = (im * 255).astype('uint8')

    sx,sy,width,height = 0,0,0,0

    img_g, faces = facial_recognition(im)

    # find area of detected faces
    for (x, y, w, h) in faces:
        sx,sy = x,y
        width,height = w,h
        face = image[sy:sy+height, sx:sx+width] # link faces to image
        # blur part of image where faces are detected
        face = blur(face, n=200)
        if "float" in str(face.dtype):
            face = (face * 255).astype('uint8')
        im[sy:sy + height, sx:sx + width] = face

    return im


def facial_recognition(im):
    """
    This function finds the faces in the image using a haar classifier

    Arguments:
        im: ndarray(uint8); source image

    Returns:
        original image as grayscale and matrices of faces
    """
    # converts image to grayscale using OpenCV
    img_g = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)

    # defines path to haar classifier
    haar = 'haarcascade_frontalface_default.xml'
    # creates the haar cascade
    haar_cascade = cv2.CascadeClassifier(haar)
    #
    faces = haar_cascade.detectMultiScale(
        img_g,
        scaleFactor=1.1,
        minNeighbors=5
    );

    return img_g, faces

def inpainting_demo(im):
    """
    This function is built as an example of inpainting. The difference between
    this function and the inpainting function is that this one actually destroys
    an image to be rconstructed for demonstration purposes.

    Arguments:
        im: ndarray(float64): user provided image

    Returns:
        orginal image, destroyed image, reconstructed image
    """
    if len(im.shape) != 3:
        print("I need a coloured image to run..")
        return False
    # take a copy of the image and destroy it
    destroyed = im.copy()
    # make hevery other horizontal line black
    destroyed[::2] = 0

    mask = np.ones(destroyed.shape[:2])
    mask = mask.astype(bool)
    mask[::2] = False

    fixed       = destroyed.copy()
    image_lines = destroyed.copy()

    for _ in range(20):
        destroyed = blur(destroyed, n=1)

        destroyed[destroyed > 1] = 1
        destroyed[destroyed < 0] = 0

        fixed[~mask] = destroyed[~mask]
        destroyed[mask] = fixed[mask]

    return im, image_lines, fixed

def inpainting(im, mask, iters):
    """
    This function reconstructs (a) user selected area(s) of a provided image

    Arguments:
        im: ndarray(float64): user provided image
        mask: ndarray(bool): user provided mask for section of image where the
                             function should operate.
        iters: int: number of blur function calls

    Returns:
        the working-set image
    """
    # Make a copy of the image and call it the working-set
    ws = im.copy()
    for _ in range(iters):
        ws = blur(ws, n=1)
        ws[ws > 1] = 1
        ws[ws < 0] = 0
        im[~mask] = ws[~mask]
        ws[mask] = im[mask]

    return ws

# grayscale image
def demosaicing(im):
    """
    Extracts the colors of an user selected image to create a grayscale mosaic
    of the image. The function then builds separate R,G and B images to
    reconstruct a color image from the grayscale mosaic

    Arguments:
        im: ndarray(float64); source image

    Returns:
        original image and reconstructed image
    """

    if len(im.shape) != 3:
        print("I need a coloured image to run..")
        return False

    # create mosaic
    mosaic = np.zeros(im.shape[:2])
    mosaic[ ::2, ::2] = im[ ::2, ::2, 0]
    mosaic[1::2, ::2] = im[1::2, ::2, 1]
    mosaic[ ::2,1::2] = im[ ::2,1::2, 1]
    mosaic[1::2,1::2] = im[1::2,1::2, 2]

    r_image = np.zeros(im.shape[:2])
    g_image = np.zeros(im.shape[:2])
    b_image = np.zeros(im.shape[:2])

    # assign color pixels to color image
    r_image[ ::2, ::2] = mosaic[ ::2, ::2]
    g_image[1::2, ::2] = mosaic[1::2, ::2]
    g_image[ ::2,1::2] = mosaic[ ::2,1::2]
    b_image[1::2,1::2] = mosaic[1::2,1::2]

    # create R,G,B masks for inpainting
    r_mask = np.zeros(im.shape[:2]).astype(bool)
    g_mask = np.zeros(im.shape[:2]).astype(bool)
    b_mask = np.zeros(im.shape[:2]).astype(bool)
    r_mask[ ::2, ::2] = True
    g_mask[1::2, ::2] = True
    g_mask[ ::2,1::2] = True
    b_mask[1::2,1::2] = True

    # run inpaint function where there is no color values
    r_image = inpainting(r_image, mask=r_mask, iters=100)
    g_image = inpainting(g_image, mask=g_mask, iters=100)
    b_image = inpainting(b_image, mask=b_mask, iters=100)

    # setup color image
    y,x = im.shape[:2]
    color_image = np.zeros((y,x,3),np.float64)

    # insert RGB values to color image
    color_image[:,:,0] = r_image
    color_image[:,:,1] = g_image
    color_image[:,:,2] = b_image

    return im, color_image

# if I have the time....
def clone(source, target, view, target_area, dt=.2):
    """
    This function takes a view of the source image and copies it over to
    a target image. The view of the source image will be pasted into the
    tagret after applying the Poisson-equation.

    Arguments:
        source: ndarray(float64): image where the desired object is located
        target: ndarray(float64): image to where the desired object should be
                                  copied over to
        view: matrix(int): user provided section of object from source image
        target_area: matrix(int): square location for placement of object to
                                  target image
        dt: float: constant, usually 0.2

    Returns:
        The seamlessly cloned target image
    """

    source = imread('shark.jpg').astype(float)/255
    target = imread('reef.jpg').astype(float)/255

    x_start, x_end = 523, 1200
    y_start, y_end = 270, 627
    x_size = x_end - x_start
    y_size = y_end - y_start
    #
    source_view = source[y_start:y_end, x_start:x_end]
    target_area = target[0:0+y_size, 1130:1130+x_size]

    for _ in range(300):
        u_1 = laplacian(source_view)
        u_0 = laplacian(target_area)
        target_area += dt*(u_0 - u_1)
        target_area[target_area > 1] = 1
        target_area[target_area < 0] = 0

    plt.imshow(target)
    plt.axis('off')
    plt.show()


if __name__ == "__main__":
    main()
