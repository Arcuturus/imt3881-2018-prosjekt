import unittest
from poisson import *
import numpy as np

class tests(unittest.TestCase):

    def test_laplacian_shape(self):
        test_matrix = np.zeros((3,3,3))
        self.assertEqual(laplacian(test_matrix).shape,test_matrix.shape)

    def test_laplacian_values(self):
        test_matrix = np.matrix([[3,2,3],[9,2,3],[4,2,2]], dtype=np.float64)
        # manually calculated matrix with neuman boundaries
        true_matrix = np.matrix([[8,8,8],[8,8,8],[8,8,8]], dtype=np.float64)
        self.assertTrue(np.array_equal(laplacian(test_matrix),true_matrix))

    def test_gradient_forward_x_values(self):
        test_matrix = np.matrix([[3,2,3],[9,2,3],[4,2,2]], dtype=np.float64)
        gradient_f_x = np.matrix([[-1,1,1],[-7,1,1],[-2,0,0]], dtype=np.float64)
        actual_matrix, _  = gradient(test_matrix, 'f')
        self.assertTrue(np.array_equal(actual_matrix, gradient_f_x))

    def test_gradient_forward_y_values(self):
        test_matrix = np.matrix([[3,2,3],[9,2,3],[4,2,2]], dtype=np.float64)
        gradient_f_y = np.matrix([[6,0,0],[-5,0,-1],[-5,0,-1]], dtype=np.float64)
        _, actual_matrix = gradient(test_matrix, 'f')
        self.assertTrue(np.array_equal(actual_matrix, gradient_f_y))

    def test_gradient_backward_x_values(self):
        test_matrix = np.matrix([[3,2,3],[9,2,3],[4,2,2]], dtype=np.float64)
        gradient_b_x = np.matrix([[-1,-1,1],[-7,-7,1],[-2,-2,0]], dtype=np.float64)
        actual_matrix, _  = gradient(test_matrix, 'b')
        self.assertTrue(np.array_equal(actual_matrix, gradient_b_x))

    def test_gradient_backward_x_values(self):
        test_matrix = np.matrix([[3,2,3],[9,2,3],[4,2,2]], dtype=np.float64)
        gradient_b_y = np.matrix([[6,0,0],[6,0,0],[-5,0,-1]], dtype=np.float64)
        _, actual_matrix  = gradient(test_matrix, 'b')
        self.assertTrue(np.array_equal(actual_matrix, gradient_b_y))
