# IMT3881 2018 Prosjekt

Dette programmet tar for seg forskjellige løsninger av Poisson-ligningen oversatt til Python. I opgaven har jeg valgt å løse de forskjellige problemene:

* Glatting
* Kontrastforsterkning (lineær, ikkelineær)
* Konvertering av farge til gråskala
* Anonymisering av ansikter
* Inpainting
* Rekonstruering av fargebilder vha gråskalamosaikk
* Kantbevarende glatting
* Sømløs kloning

## Praktisk informasjon

* Klon dette prosjektet inn i selvvalgt mappe på maskinen
* CD inn i mappen
* Kjør pip3 install -r requirements.txt for å hente alle pakkene som trengs for å kjøre programmet
* Kjør programmer med python3 main.py

## Om programmet

Når du kjører programmet blir møtt av en enkel meny. Her kan du selv velge hva du ønsker å gjøre. Etter valgt alternativ vil du kunne velge hva slags bilde du vil gjøre operasjonen på. Du må være obs på at bildet blir representert som absolutt-sti,
så det enkleste er å legge bildene til en undermappe. Unntak er:
* 9: Kloning. Her bruker jeg to hardkodede bilder. Dette var kun for demonstrasjon.

Ved videre utvikling vil jeg lage en enkel grafisk interface og la brukeren endre på flere variabler.

## Testing
Jeg har lagd noen få tester, spesifikt for å teste laplacian og gradienten. Disse kan kjøres med: Python -m unittest unit_testing.py